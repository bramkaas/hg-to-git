# Copyright (C) ALbert Mietus,part of MESS.TechPush 2018
# -*- coding: utf-8 -*-

# read STD config ...
#==========================================
import sys; sys.path.append('../_external_templates/conf')
from YP_HT_SE_conf import *

# General information about the project.
#======================================
project = 'MESS TechPush'
copyright = "2018; ALbert Mietus & students"

show_authors = True

# Overrule std_conf, where needed
#================================
from datetime import datetime
version = "DRAFT"                                                       # |version|
build = datetime.now().strftime("%Y.%m.%d")
release = "%s-%s" % (version, build)                                    # |release|


release = version
html_title = project + " | " + release # DEFAULT: '<project> v<revision> documentation' -- Strip "documentation"


# plantUML
#---------
extensions.append('sphinxcontrib.plantuml')
plantuml = 'plantuml'
