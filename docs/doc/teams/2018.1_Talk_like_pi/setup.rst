setup
=====



To make the round trip and bandwidth applications run some sort of setup is needed. There are different ways to make this setup like using virtual machines. 
Because there are four raspberries on hand and its more fun they are used to 

.. figure:: img/berry.jpg
   :scale: 10%
   
   
The image above shows how i do it. four raspberries connected to a router.
The DHCP setting can easily be adjusted to give the raspberries static IP's. 
The following ip addresses are set for the raspberries:

   * 192.168.0.200
   * 192.168.0.201
   * 192.168.0.202
   * 192.168.0.203   

The raspberry i work from is 202, theres no real reason for it just that its the raspberry that i used
first. However it is important to also work from this pi, because the shell scripts that have been made
assume this setup and that the pi from which it is started has ip 202. For these scripts to work each of the 
raspberries needs a ssh-key without password from the 202 raspberry. Though this is only neccessary to use the 
isc programs. 

It is also important to have DDS, MQTT and ZMQ installed on all the raspberries, the respository onlye needs to be cloned to one of them. How to isntall all three libraries is
shown on the main page of this documentation under IPSC Inter Process & System Communications.
