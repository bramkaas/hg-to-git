MQTT
====

MQTT(Message Queuing Telemetry Transport) uses a pub-sub method to get messages from client to client, more on that in the chapter MQTT workings.



Running MQTT
************

before running anything the following ASSUMPTIONS are made here:

   * MQTT is installed as per the IPSC manual
   * you have four raspberries configured as per the setup chapter
   * the repository has been downloaded
   
Now setting up the first MQTT roundtrip is very easy::

   sh <techpush-sourcedir>/IPSC/MQTT/py/RoundTrip/ipc/run.sh

now the local roundtrip is being setup up and run. Be sure to run cleanup.sh after cancelling the script.
note: Some of the shell scripts automatically run cleanup.sh when ctrl-c is pressed

running the other MQTT setups is just as easy. For example the isc version of the roundtrpp::
   
    sh <techpush-sourcedir>/IPSC/MQTT/py/RoundTrip/isc/run.sh

or for both the bandwith programs::
   
   sh <techpush-sourcedir>/IPSC/MQTT/py/Bandwidth/ipc/run.sh
   sh <techpush-sourcedir>/IPSC/MQTT/py/Bandwidth/isc/run.sh

The MQTT.py program takes command line inputs in the following order:

Receiving topic
Sending topic
MQTT broker IP
a 1 or 0 for being a master or hub respectively
QOS quality of service 

so for example::

    python3 MQTT.py ReceivingTopic SendingTopic 192.168.0.202 1 0

In the example a master(1) is started which sends to SendTopic, receives on ReceiveTopic,
works with a broker on 192.168.0.202 and uses a QOS of 0.



MQTT workings
*************

MQTT works by sending a message to a central broker which then distributes the message to everybody thats listening in. It looks something like:

.. uml::

   @startuml
   
   Client1 -> Broker : message
   Broker -> Client2 : message
   Broker -> Client3 : message
   Broker -> Client4 : message
   
   
   @enduml

In the context of the roundtrip this looks like: 

.. uml::

   @startuml
   
   master   -> broker   : message[1]
   broker   -> hub1     : message[1]
   hub1     -> broker   : message[1]
   broker   -> hub2     : message[1]
   hub2     -> broker   : message[1]
   broker   -> hub3     : message[1]
   hub3     -> broker   : message[1]
   broker   -> master   : message[1]
   
   master   -> broker   : message[2]
   broker   -> hub1     : message[2]
   hub1     -> broker   : message[2]
   broker   -> hub2     : message[2]
   hub2     -> broker   : message[2]
   broker   -> hub3     : message[2]
   hub3     -> broker   : message[2]
   broker   -> master   : message[2]
   
   master   -> broker   : message[3]
   broker   -> hub1     : message[3]
   hub1     -> broker   : message[3]
   broker   -> hub2     : message[3]
   hub2     -> broker   : message[3]
   broker   -> hub3     : message[3]
   hub3     -> broker   : message[3]
   broker   -> master   : message[3]
   
   
   @enduml
   
In the context of a roundtrip this method might seem a bit inefficients as every message has to go throught the broker. Also there is a sinle point of failure. However if the message needs to go to multiple 
hubs it costs almost nothing extra for the whole system. Speed should remain the same. This can be useful if for example there is a system where one sensor input needs to go to several different actuators. 
There are many more downsides and upsides however i will not discuss them here, there are many websites that discuss them like this one: http://portals.omg.org/dds/features-benefits/
