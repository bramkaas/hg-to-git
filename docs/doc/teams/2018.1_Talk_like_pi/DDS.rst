DDS
===

DDS(Dynamic Data Distribution) 


Running DDS
***********

Like for MQTT and ZMQ, DDS also needs some preparations to use: 

   * ZMQ is installed as per the IPSC manual
   * you have four raspberries configured as per the setup chapter
   * the repository has been downloaded

For DDS there is a shell script that sets up a ping pong within the RoundTime folder like::

   sh <techpush-sourcedir>/IPSC/DDS/CycloneDDS/py/RoundTime/run.sh
   
This will make a ping pong between two raspberry pi's right now, however it is possible to make a round trip using the 
python scripts. This can be done like this::
 
   python3 <techpush-sourcedir>/IPSC/DDS/CycloneDDS/py/RoundTime/DDShub.py hub1 hub2
   python3 <techpush-sourcedir>/IPSC/DDS/CycloneDDS/py/RoundTime/DDShub.py hub2 hub3
   python3 <techpush-sourcedir>/IPSC/DDS/CycloneDDS/py/RoundTime/DDShub.py hub3 master
   python3 <techpush-sourcedir>/IPSC/DDS/CycloneDDS/py/RoundTime/DDSmaster.py master hub1
   
these command can be run on the same or seperate systems and they still work. DDS will work out how to connect them, of course the systems do have to be on the same
network. 

In short what happens here is that first three DDS-hubs are started. the first argument given is the topic the hub is listening to for incoming messages,
the second input is the topic to which the received message will be sent. The master functions basicly the same way as the hub with only two differences:
it adds 1 to the message each time it passes and some time after it started it sends the first message.


