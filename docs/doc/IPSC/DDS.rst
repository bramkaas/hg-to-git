.. (C) ALbert Mietus, dropjes-licentie: copy & use, and betaal met dropjes naar nuttigheid

==============================
DDS: Data Distribution Service
==============================

  DDS is a standard, realtime, platform & language agnostic, distributed communication-system; based on
  pub-sub. It was developed by/for Thales radar systems; then called “splice”.
  Later become a standard, with several vendors, both commercially and opensource. It is widely
  used: in finance, technical and scientific environments. It is also very suited for and populair in
  the IoT world. Also see: https://en.wikipedia.org/wiki/Data_Distribution_Service

Recently, the Eclipse foundation has approved an proposal to have a (true) open-source
implementation, called `Cyclone DDS
<https://projects.eclipse.org/proposals/eclipse-cyclone-dds>`_. It is based on a Community Edition
of a commercial vendor.

* All sources are on `github <https://github.com/eclipse/cyclonedds>`_.
* There is also a `python-binding <https://github.com/atolab/python-cdds>`_ (also on github).
* Java and other language bindings are probably available, or upcoming.

Cyclone DDS (cdds)
==================

This implementation is used for the IPSC experiments (other are tried, but not (yet) working).

* It does work on my Mac, and the RaspberryPi (Model 2, “stretch” (9.1 Debian)
* All code is natively compiled, both on Mac and RaspberryPi (no cross-compiler needed!)
* Documentation is available, but limited. And often not updated to the Cyclone version.

.. note:: Don’t mix it up!

   CycloneDDS is based on an earlier “community edition” of ADLINK; which has also put other versions
   on github; mostly in there `“atolab” <https://github.com/atolab/>`_ account. Although related;
   they are not the same!

   * In some cases, there docs (and code) is *almost* the same. Or point to each other.
   * This is confusing; be warned!
   * An earlier version of this document even pointed to the wrong repro :-)
   * The python-binding *python-cdds* is even part of the atolab account.
   * That account has another python-binding (pydds) too; that is *NOT* working/compatible


Build & Install
===============

.. highlight:: console

Base (C/C++)
------------

Just clone the sources, run cmake and install; as described on https://github.com/eclipse/cyclonedds

Before one can compile it, one should install cmake (note make!), Marvin and Java-8 (or newer)

On Linux/raspberrypi::

   sudo apt-get install  cmake
   sudo apt-get install  oracle-java8-jdk
   sudo apt-get install  maven
  
check if the right java version is used::
 
    java -version
It should say something like::

   java version "1.8.0_65"
   
if not, then set the proper version with::
   
    sudo update-alternatives --config java


Python bindings
---------------

Again, clone, and build; See: https://github.com/atolab/python-cdds

One needs (python3) jsonpickle (note the typo on the page: the C is missing). And cdds (see above)::

   sudo apt-get install python3
   sudo pip install jsonpickle


Links
=====
* https://projects.eclipse.org/proposals/eclipse-cyclone-dds
* https://github.com/eclipse/cyclonedds
* https://github.com/atolab/python-cdds
