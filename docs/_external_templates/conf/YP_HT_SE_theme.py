# Copyright (C) ALbert Mietus, use at will
# Sogeti (HT) theme configuration
# -*- coding: utf-8 -*-

##
## Use SgHT colors/fonts
##
_SgHT_GRIJS  = '#3F3F3F'
_SgHT_ZWART  = '#000000'
_SgHT_ROOD   = '#FF4019'
_SgHT_dROOD  = '#8C0800'
_SgHT_PAARS1 = '#850C70'
_SgHT_GEEL   = '#FFD32B'
_SgHT_GROEN  = '#008444'
_SgHT_PAARS2 = '#6A2C91'
_SgHT_ORANJE = '#FC9200'
_White  = '#ffffff'

html_theme_options = {
    ## Options
    'collapsiblesidebar' : True,
    'externalrefs'       : True,
    'stickysidebar'      : True,

    ## fonts # (CSS font-family):
    'bodyfont' : '"Georgia", "Times New Roman", "Times"',
    'headfont' : '"Georgia", "Times New Roman", "Times"',

    ## Color's (CSS color)
    'bgcolor'		: _White,	#  Body background color.
    'textcolor'		: _SgHT_ZWART,	#  Body text color.
    'linkcolor'		: _SgHT_dROOD,	#  Body link color.
    'visitedlinkcolor'	: _SgHT_GRIJS,	#  Body color for visited links.

    'headbgcolor'	: _White,	#  Background color for headings.
    'headtextcolor'	: _SgHT_dROOD,	#  Text color for headings.
    'headlinkcolor'	: _SgHT_dROOD,	#  Link color for headings.

    'sidebarbgcolor'	: _SgHT_dROOD,	#  Background color for the sidebar.
    'sidebartextcolor'	: _White, 	#  Text color for the sidebar.
    'sidebarlinkcolor'	: _SgHT_GEEL,	#  Link color for the sidebar.
    'sidebarbtncolor'	: _SgHT_dROOD,	#  Background color for the sidebar collapse button (used when collapsiblesidebar is true).

    'relbarbgcolor'	: _SgHT_dROOD,	#  Background color for the relation bar(s). ( high & low h-bars)
    'relbartextcolor'	: _White,	#  Text color for the relation bar.
    'relbarlinkcolor'	: _White,	#  Link color for the relation bar.

    'codebgcolor'	: _SgHT_ORANJE,	#  Background color for code blocks.
    'codetextcolor'	: _SgHT_PAARS2,	#  Default text color for code blocks, if not set differently by the highlighting style.

    'footerbgcolor'	: _SgHT_GRIJS,	#  Background color for the footer line.
    'footertextcolor'	: _SgHT_GROEN,	#  Text color for the footer line.

    }
