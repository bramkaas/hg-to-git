# Copyright (C) ALbert Mietus, SoftwareBeterMaken.nl; 2011-2016
#  STANDARD CONFiguration for Sphinx-doc
# -*- coding: utf-8 -*-

print("Using 'YP_HT_SE' settings")

###
### File/Project layout
###

master_doc        = 'index'
source_suffix     = '.rst'
exclude_patterns  = ['**/.#*', '**/_*']
html_static_path  = ['../_external_templates/static/']
templates_path    = ['../_external_templates/templates']


###
### Kick off
###
try:    extensions = extensions
except NameError: extensions=[]

show_authors = True

rst_epilog = None
rst_prolog = """
.. include:: /_generic.inc

"""

from YP_HT_SE_theme import *

###
### Normal HTML output
###


html_static_path.append ('_static/')
templates_path.append('_templates')

html_sidebars = { '**': [
    'globaltoc.html',
    'searchbox.html',
    'relations.html',
    'sourcelink.html',
]}

html_theme = 'classic'
#html_style = 'SgHT-sphinx.css'



# sphinx.ext.todo
#-----------------

extensions.append('sphinx.ext.todo')

todo_include_todos=True



# sphinx.ext.autodoc, sphinxcontrib.napoleon
#-------------------------------------------

extensions.append('sphinx.ext.autodoc')
extensions.append('sphinxcontrib.napoleon') # For google- & NumPy-style docstrings

autodoc_default_flags     = ['members', 'show-inheritance']
autoclass_content         = "both"
autodoc_member_order      = "bysource"

napoleon_google_docstring = True
napoleon_numpy_docstring  = False
napoleon_use_ivar         = True
napoleon_use_rtype        = True
