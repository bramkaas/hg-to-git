#!/usr/bin/env python3
# (C) ALbert Mietus, dropjes-licentie: copy & use, and betaal met dropjes naar nuttigheid

import zmq

import sys
from datetime import datetime

MAX=5*1000*10
EVERY=5*1000

def ping(socket, counter):
    socket.send_string("Ping %i" % counter)


def pong(socket):
    msg = socket.recv().decode()
    return int(msg.split(' ')[1])


def report(c, t0, done=None):
    t1 = datetime.now()
    t = (t1-t0).total_seconds()
    print("Ping %s %i messages, in %2.1f seconds: %i msg/sec [%3.3f ms/msg]" % (
        (done if done else "already"), c, t, c/t, t*1000/c))


def main():
    port =  int(sys.argv[1]) if len(sys.argv) > 1 else 8888
    context = zmq.Context()
    socket = context.socket(zmq.PAIR)                              # XXX GAM: hardcoded, use also other network-patterns
    socket.connect("tcp://localhost:%s" % port)                    # XXX GAM: hardcoded, use also other protocols

    print("[Pong] Ping to port: %s" % port)
    t0 = datetime.now()

    for i in range(1,MAX+1):
        ping(socket, i)
        assert pong(socket) == i

        if i % EVERY == 0:
            report(i, t0)

    report(i,t0, "klaar")

if __name__ == "__main__":
    main()
