#!/usr/bin/env python3
# (C) ALbert Mietus, dropjes-licentie: copy & use, and betaal met dropjes naar nuttigheid

import zmq

import sys
from datetime import datetime

def echo(socket):
    msg = socket.recv().decode()
    count = int(msg.split(' ')[1])
    socket.send_string("Echo %i" % count)


def main():
    port =  int(sys.argv[1]) if len(sys.argv) > 1 else 8888
    context = zmq.Context()
    socket = context.socket(zmq.PAIR)                              # XXX GAM: hardcoded, use also other network-patterns
    socket.bind("tcp://*:%s" % port)                               # XXX GAM: hardcoded, use also other protocols
    print("[Ping] Echo server started on port %s" % port)

    while True:
        echo(socket)


if __name__ == "__main__":
    main()
