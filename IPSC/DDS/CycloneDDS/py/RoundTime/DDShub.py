import cdds as dds

import time
from  _pingpong import Ping
import sys


#SendingTopic = 'master2'
#ReceivingTopic = 'master1'

ReceivingTopic = sys.argv[1] if len(sys.argv) >1 else 'hub'
SendingTopic = sys.argv[2] if len(sys.argv) >2 else 'master'




def send_data(w, val):
    s = Ping(val)
    w.write(s)

def on_receive_data(receiver):
    samples = receiver.take(dds.all_samples())
    for s in samples:                                                   # XXX GAM: Assume it's it only 1 ....
        if s[1].valid_data:
            ping = s[0]
            val = ping.val
            send_data(sender, val)
        else:
            print(s[0], s[1])


if __name__ == '__main__':
    global receiver, sender, val
    val=None

    rt = dds.Runtime()
    dp = dds.Participant(0) 
    topic1  = dds.FlexyTopic(dp,  ReceivingTopic) # set receiving topic
    topic2  = dds.FlexyTopic(dp,  SendingTopic) # set sending topic
    
    receiver  = dds.FlexyReader(dp, topic1, on_receive_data, [dds.Reliable(), dds.KeepLastHistory(10)])
    sender  = dds.FlexyWriter(dp, topic2, [dds.Reliable(), dds.KeepLastHistory(10)])
    print("start listening")
    while True:
        pass