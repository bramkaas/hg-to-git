#! /usr/bin/env python3

import time
from datetime import datetime
import cdds as dds
from  _pingpong import Ping
import sys

ReceivingTopic = sys.argv[1] if len(sys.argv) >1 else 'master'
SendingTopic = sys.argv[2] if len(sys.argv) >2 else 'hub'


def send_data(w, val):
    s = Ping(val)
    w.write(s)

def report(c, t0, done=None):
    t1 = datetime.now()
    t = (t1-t0).total_seconds()
    print("Ping %s %i messages, in %2.1f seconds: %i msg/sec [%3.3f ms/msg]" % (
        (done if done else "already"), c, t, c/t, t*1000/c))

def on_receive_data(receiver):
    global val
    samples = receiver.take(dds.all_samples())
    for s in samples:                                                   # XXX GAM: Assume it's it only 1 ....
        if s[1].valid_data:
            ping = s[0]
            val = ping.val
            send_data(sender, val+1)
        else:
            print(s[0], s[1])


EVERY = 1000
SLEEP = 10

if __name__ == "__main__":
    global receiver, sender, val
    val = None
    
    rt = dds.Runtime()
    dp = dds.Participant(0)
    topic1  = dds.FlexyTopic(dp,  ReceivingTopic) # set receiving topic
    topic2  = dds.FlexyTopic(dp,  SendingTopic) # set sending topic
    
    

    receiver  = dds.FlexyReader(dp, topic1, on_receive_data, [dds.Reliable(), dds.KeepLastHistory(10)])
    sender  = dds.FlexyWriter(dp, topic2, [dds.Reliable(), dds.KeepLastHistory(10)])
    print("Wait a bit ...")
    time.sleep(SLEEP)

    print("START: Ping 1 ...")
    t0 = datetime.now()
    send_data(sender, 1)
    
    while True:
        time.sleep(SLEEP)
        report(val, t0)
    
    
    
    
    