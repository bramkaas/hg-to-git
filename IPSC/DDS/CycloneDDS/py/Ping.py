#!/usr/bin/env python3

import cdds as dds

import time
from  _pingpong import Pong

SLEEP=6

def pong(w, val):
    s = Pong(val)
    w.write(s)


def on_ping(r):
    global w, val

    samples = r.take(dds.all_samples())
    for s in samples:                                                   # XXX GAM: Assume it's it only 1 ....
        if s[1].valid_data:
            ping = s[0]
            val = ping.val
            pong(w, val)
        else:
            print(s[0], s[1])

if __name__ == '__main__':
    global r,w, val
    val=None

    rt = dds.Runtime()
    dp = dds.Participant(0)
    t1  = dds.FlexyTopic(dp,  'Ping2Pong')
    t2  = dds.FlexyTopic(dp,  'Pong2Ping')

    w  = dds.FlexyWriter(dp, t1, [dds.Reliable(), dds.KeepLastHistory(10)])
    r  = dds.FlexyReader(dp, t2, on_ping, [dds.Reliable(), dds.KeepLastHistory(10)])

    while True:
        time.sleep(SLEEP)
        print("%i seconds later, we are at: %s" % (SLEEP, val))

