#! /usr/bin/env python3

import time
from datetime import datetime
import cdds as dds
from  _pingpong import Ping

EVERY = 1000
SLEEP = 10

def report(c, t0, done=None):
    t1 = datetime.now()
    t = (t1-t0).total_seconds()
    print("Ping %s %i messages, in %2.1f seconds: %i msg/sec [%3.3f ms/msg]" % (
        (done if done else "already"), c, t, c/t, t*1000/c))

def ping(w, val):
    s = Ping(val)
    w.write(s)

def on_pong(r):
    global w, val

    samples = r.take(dds.all_samples())
    for s in samples:                                                   # XXX GAM Assume it's it only 1 ....
        if s[1].valid_data:
            pong = s[0]
            val = pong.val
            ping(w, val+1)
            if val % EVERY == 0:
                report(val, t0)


if __name__ == '__main__':
    global r,w, val
    val=None

    rt = dds.Runtime()
    dp = dds.Participant(0)
    t1  = dds.FlexyTopic(dp,  'Ping2Pong')
    t2  = dds.FlexyTopic(dp,  'Pong2Ping')

    w  = dds.FlexyWriter(dp, t2, [dds.Reliable(), dds.KeepLastHistory(10)])
    r  = dds.FlexyReader(dp, t1, on_pong, [dds.Reliable(), dds.KeepLastHistory(10)])

    print("Wait a bit ...")
    time.sleep(SLEEP)

    print("START: Ping 1 ...")
    t0 = datetime.now()
    ping(w,1)


    while True:
        time.sleep(SLEEP)
        report(val, t0)
